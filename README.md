# Dynamic tsec generic DP

Un componente para realizar peticiones a endpoints que requieran envio de tsec, en caso de caducar, debe obtener un nuevo tsec y continuar la petición

`añade soporte para peticiones asincronas`

**Requiere la abstracción de cualquier tipo de petición http a modo de encapsulamiento, que permita un manejo nativo y "familiar" de las peticiones**

## Uso

La función `core` permite la simulación de llamadas asincronas con respuesta binaria aleatora

Ejemplo de uso con la api de `json placeholder`
```js
core('4').then(response => response.json()).then(data => console.log(data.title))
```

**NOTA** Para poder probar el correcto funcionamiento, ejectuar la función `core` hasta que el valor de retorno de la función `tokenVerification` sea **false** y continuar haciendo peticiones para ver el comportamiento con peticiones asincronas

**IMPORTANTE** El parametro de core debe ser un numero entre comillas simples, de este modo se hará la petición a un post especifico de la API de *JSON Placeholder*

## Comportamiento

```mermaid 
graph TB

A(Nueva petición) -- entra --> B(CORE)
B --> H{Peticiones en pausa}
H -- NO --> K(Ejecuta petición)
K -- verifica --> G{tsec valido}
H -- SI --> F
G -- SI --> D(Propagación de la respuesta)
G -- NO --> E(Peticiones en pausa)
E --> I{Petición nuevo tsec}
E -. petición entra .-> F(Stack)
E -- cierra peticiones entrantes --> B
I -- SUCCESS --> J(Actualiza tsec) 
J -- libera -->F
J -- abre peticiones entrantes -->B
I -- NO --> I
F -- crea --> A
```

## Modulos por crear

- Soporte para SessionStorage 
- Soporte para multi-tsec
- Consumo fetch
- Añadir soporte agnostico al endpoint 
- Comenzar separación en posibles exports para dar mantenimiento

## Checksum

- 08/01/2018: Se añade soporte para consumos reales usando fetch y promesas realmente "dormidas" hasta que llegue el tsec

