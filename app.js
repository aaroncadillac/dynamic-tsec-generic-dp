var door = true;
var token = "token";
var sleeper;

function requestWrapper(p) {
  console.log('wrapper', p)
  return new Promise(resolve => {
    setTimeout(p => {
      resolve(!!Math.floor(Math.random() * 2))
    }, 1000)
  })
}

function getNewToken() {
  console.warn('getting new token...');
  return new Promise(resolve => {
    setTimeout(p => {
      let time = new Date();
      resolve(time.getTime());
    }, 15000)
  })
}

async function tokenVerification(p) {
  console.log('Initialing Verification...');
  let valid = await requestWrapper(p);
  console.log('tokenVerification, valid is', valid)
  if(valid) return true;
  else{
    console.log('Setting door to false');
    door = false;
    return false;
  }
}
function printStack() {
  stack.forEach((value) => {
    console.log(value);
  });
}
async function core(endpoint) {
  if (door) {
    if(response = await tokenVerification(endpoint)) {
      console.log('success core for ', endpoint)
      return fetch("https://jsonplaceholder.typicode.com/posts/"+endpoint)
    }
    else {
      console.warn('fail core for ', endpoint)
      token = await (sleeper = getNewToken());
      console.info('token change to ', token)
      door = true;
      console.info('door is true');
      return fetch("https://jsonplaceholder.typicode.com/posts/"+endpoint)
    }
  }
  else {
    await sleeper;
    return fetch("https://jsonplaceholder.typicode.com/posts/"+endpoint)
  }
}
